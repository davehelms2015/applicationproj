﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework.Constraints;

namespace TransportProj
{
    public static class CarFactory
    {
        public static Car Get(CarEnum type)
        {
            switch (type)
            {
                case CarEnum.Sedan:
                    return new Sedan(0, 0, null, null);
                case CarEnum.RaceCar:
                    return new RaceCar(0, 0, null, null);
                default:
                    return new Sedan(0, 0, null, null);
            }
        }
    }
}
