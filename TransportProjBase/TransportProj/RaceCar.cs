﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    class RaceCar : Car
    {
        public RaceCar(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
        }

        /*
         * Correct overshoot error by only moving one position once
         */
        public override void MoveUp()
        {
            if (YPos < City.YMax - 1)
            {
                YPos += 2;
                WritePositionToConsole();
            }
        }

        public override void MoveDown()
        {
            if (YPos > 1)
            {
                YPos -= 2;
                WritePositionToConsole();
            }
        }

        public override void MoveRight()
        {
            if (XPos < City.XMax - 1)
            {
                XPos += 2;
                WritePositionToConsole();
            }
        }

        public override void MoveLeft()
        {
            if (XPos > 1)
            {
                XPos -= 2;
                WritePositionToConsole();
            }
        }

        public override void AdjustXPos(Passenger passenger)
        {
            if (XPos < passenger.StartingXPos) XPos++;
            else XPos--;
        }

        public override void AdjustYPos(Passenger passenger)
        {
            if (YPos < passenger.StartingYPos) YPos++;
            else YPos--;
        }
    }
}
