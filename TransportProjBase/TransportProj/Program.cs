﻿using System;

namespace TransportProj
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Random rand = new Random();
            int CityLength = 10;
            int CityWidth = 10;

            City MyCity = new City(CityLength, CityWidth);
            Car car = MyCity.AddCarToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1));
            Passenger passenger = MyCity.AddPassengerToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), rand.Next(CityLength - 1), rand.Next(CityWidth - 1));

            while(!passenger.IsAtDestination())
            {
                Tick(car, passenger);
            }

            return;
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        public static void Tick(Car car, Passenger passenger)
        {
            //Deal with potential overshoot problem with RaceCar from moving two steps at once
            if (car is RaceCar && car.Passenger == null)
            {
                if (car.XPos % 2 != passenger.StartingXPos % 2)
                {
                    car.AdjustXPos(passenger);
                    return;
                }
                if (car.YPos % 2 != passenger.StartingYPos % 2)
                {
                    car.AdjustYPos(passenger);
                    return;
                }
            }

            if (car is RaceCar && car.Passenger != null)
            {
                if (car.XPos % 2 != passenger.DestinationXPos % 2)
                {
                    car.AdjustXPos(passenger);
                    return;
                }
                if (car.YPos % 2 != passenger.DestinationYPos % 2)
                {
                    car.AdjustYPos(passenger);
                    return;
                }
            }

            if (car.Passenger == null)
            {
                MoveTowardPassenger(car, passenger);
            }
            else
            {
                MoveTowardDestination(car, passenger);
            }
        }

        private static void MoveTowardPassenger(Car car, Passenger passenger)
        {
            if (passenger.StartingXPos > car.XPos)
            {
                car.MoveRight();
                return;
            }
            if (passenger.StartingXPos < car.XPos)
            {
                car.MoveLeft();
                return;
            }
            if (passenger.StartingYPos > car.YPos)
            {
                car.MoveUp();
                return;
            }
            if (passenger.StartingYPos < car.YPos)
            {
                car.MoveDown();
                return;
            }

            car.PickupPassenger(passenger);
            passenger.GetInCar(car);
        }

        private static void MoveTowardDestination(Car car, Passenger passenger)
        {
            if (passenger.DestinationXPos > car.XPos)
            {
                car.MoveRight();
                return;
            }
            if (passenger.DestinationXPos < car.XPos)
            {
                car.MoveLeft();
                return;
            }
            if (passenger.DestinationYPos > car.YPos)
            {
                car.MoveUp();
                return;
            }
            if (passenger.DestinationYPos < car.YPos)
            {
                car.MoveDown();
                return;
            }

            passenger.GetOutOfCar();
        }
    }
}
