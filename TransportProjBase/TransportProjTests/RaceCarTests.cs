﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TransportProj;

namespace TransportProjTests
{
    [TestClass]
    public class RaceCarTests
    {
        [TestMethod]
        public void RaceCarHappyPathPickUpDropOff()
        {
            int CityLength = 10;
            int CityWidth = 10;

            City MyCity = new City(CityLength, CityWidth);
            Car car = MyCity.AddRaceCarToCity(5, 5);
            Passenger passenger = MyCity.AddPassengerToCity(3, 3, 7, 7);

            while (!passenger.IsAtDestination())
            {
                Program.Tick(car, passenger);
            }

            Assert.AreEqual(passenger.GetCurrentXPos(), 7);
            Assert.AreEqual(passenger.GetCurrentYPos(), 7);
            Assert.AreEqual(car.XPos, 7);
            Assert.AreEqual(car.YPos, 7);
        }

        [TestMethod]
        public void RaceCarOvershootPassengerTest()
        {
            int CityLength = 10;
            int CityWidth = 10;

            City MyCity = new City(CityLength, CityWidth);
            Car car = MyCity.AddRaceCarToCity(5, 5);
            Passenger passenger = MyCity.AddPassengerToCity(2, 2, 7, 7);

            while (!passenger.IsAtDestination())
            {
                Program.Tick(car, passenger);
            }

            Assert.AreEqual(passenger.GetCurrentXPos(), 7);
            Assert.AreEqual(passenger.GetCurrentYPos(), 7);
            Assert.AreEqual(car.XPos, 7);
            Assert.AreEqual(car.YPos, 7);
        }

        [TestMethod]
        public void RaceCarEdgeTest()
        {
            int CityLength = 10;
            int CityWidth = 10;

            City MyCity = new City(CityLength, CityWidth);
            Car car = MyCity.AddRaceCarToCity(1, 1);
            Passenger passenger = MyCity.AddPassengerToCity(10, 10, 7, 7);

            while (!passenger.IsAtDestination())
            {
                Program.Tick(car, passenger);
            }

            Assert.AreEqual(passenger.GetCurrentXPos(), 7);
            Assert.AreEqual(passenger.GetCurrentYPos(), 7);
            Assert.AreEqual(car.XPos, 7);
            Assert.AreEqual(car.YPos, 7);
        }
    }
}
