﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TransportProj;

namespace TransportProjTests
{
    [TestClass]
    public class SedanTests
    {
        [TestMethod]
        public void SedanShouldPickUpAndDropOffPassenger()
        {
            int CityLength = 10;
            int CityWidth = 10;

            City MyCity = new City(CityLength, CityWidth);
            Car car = MyCity.AddCarToCity(5, 5);
            Passenger passenger = MyCity.AddPassengerToCity(3, 3, 7, 7);

            while (!passenger.IsAtDestination())
            {
                Program.Tick(car, passenger);
            }

            Assert.AreEqual(passenger.GetCurrentXPos(), 7);
            Assert.AreEqual(passenger.GetCurrentYPos(), 7);
            Assert.AreEqual(car.XPos, 7);
            Assert.AreEqual(car.YPos, 7);
        }

        [TestMethod]
        public void SedanShouldPickUpAndDropOffOnEdges()
        {
            int CityLength = 10;
            int CityWidth = 10;

            City MyCity = new City(CityLength, CityWidth);
            Car car = MyCity.AddCarToCity(0, 0);
            Passenger passenger = MyCity.AddPassengerToCity(10, 10, 10, 0);

            while (!passenger.IsAtDestination())
            {
                Program.Tick(car, passenger);
            }

            Assert.AreEqual(passenger.GetCurrentXPos(), 10);
            Assert.AreEqual(passenger.GetCurrentYPos(), 0);
            Assert.AreEqual(car.XPos, 10);
            Assert.AreEqual(car.YPos, 0);
        }
    }
}
